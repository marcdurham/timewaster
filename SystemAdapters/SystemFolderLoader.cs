﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TimeWaster.Common;

namespace TimeWaster.SystemAdapters
{
    public class SystemFolderLoader : IFolderLoader
    {
        public List<string> Load (string folderPath)
        {
            var files = Directory.GetFiles (
                folderPath, 
                "*.txt", 
                SearchOption.AllDirectories);
            
            return files.ToList ();
        }

        public string GetFileName(string filePath)
        {
            return Path.GetFileName(filePath);
        }
    }
}
