﻿using System.IO;
using TimeWaster.Common;

namespace TimeWaster.SystemAdapters
{
    public class SystemFileLoader : IFileLoader
    {
        public string Read (string folderPath)
        {
            return File.ReadAllText (folderPath);
        }
    }
}
