﻿using System;

namespace TimeWaster.Common
{
    public class LogEntry
    {
        static LogEntry empty = new EmptyLogEntry ();

        public static LogEntry Empty 
        { 
            get { return empty; }
        }

        public DateTime Time;
        public TimeSpan Duration;
        public string Text;

        public virtual bool IsEmpty ()
        {
            return false;
        }
    }

    public class EmptyLogEntry : LogEntry
    {
        public override bool IsEmpty ()
        {
            return true;
        }
    }
}
