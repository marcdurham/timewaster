﻿using System.Collections.Generic;

namespace TimeWaster.Common
{
    public interface IFolderLoader
    {
        List<string> Load (string folderPath);
        string GetFileName(string filePath);
    }
}
