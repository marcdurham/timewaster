﻿using System;

namespace TimeWaster.Common
{
    public class FileParsingException : Exception
    {
        string fileName;
        Exception innerException;

        public FileParsingException (string fileName, Exception innerException) 
        {
            this.fileName = fileName;
            this.innerException = innerException;
        }

        public override string Message 
        {
            get 
            {
                return 
                    "Error parsing file: " + fileName 
                    + Environment.NewLine + "Message: "
                    + Environment.NewLine + innerException.Message;
            }
        }
    }
}
