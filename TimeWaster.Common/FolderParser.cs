﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TimeWaster.Common
{
    public class FolderParser
    {
        IFileLoader fileLoader;
        IFolderLoader folderLoader;

        public FolderParser (IFileLoader fileLoader, IFolderLoader folderLoader)
        {
            this.fileLoader = fileLoader;
            this.folderLoader = folderLoader;
        }

        public List<LogEntry> Parse (string folderpath)
        {
            var list = new List<LogEntry> ();
            List<string> filePaths = folderLoader.Load (folderpath);
            var parser = new LogParser ();

            foreach (string filePath in filePaths) 
                TryParseFile (list, parser, filePath);
            
            return list;
        }

        void TryParseFile (List<LogEntry> list, LogParser parser, string filePath)
        {
            try {
                ParseFile (list, parser, filePath);
            } catch (Exception e) {
                throw new FileParsingException (filePath, e);
            }
        }

        void ParseFile (List<LogEntry> list, LogParser parser, string filePath)
        {
            string text = fileLoader.Read (filePath);

            DateTime date = DateTime.MinValue;
            string fileName = folderLoader.GetFileName (filePath);
            if (Regex.IsMatch (fileName, @"\d\d\d\d-\d\d-\d\d.*")) {
                string datePart = fileName.Substring (0, 10);
                date = DateTime.Parse (datePart);
            }

            var entries = parser.Parse (text, date);
            list.AddRange (entries);
        }
   }
}
