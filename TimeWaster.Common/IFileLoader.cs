﻿namespace TimeWaster.Common
{
    public interface IFileLoader
    {
        string Read (string filePath);
    }
}
