﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TimeWaster.Common
{
    public class LogParser
    {
        List<LogEntry> list = new List<LogEntry> ();
        DateTime date;
        LogEntry currentEntry;

        public List<LogEntry> Parse (string input, DateTime date)
        {
            list = new List<LogEntry> ();

            if (string.IsNullOrWhiteSpace (input))
                return list;

            this.date = date;

            input = input.Replace ("\r\n", "\n");

            var lines = input.Split ('\n');

            currentEntry = new LogEntry ();

            foreach (string line in lines)
                ParseLine (line);

            return list;
        }

        void ParseLine (string line)
        {
            bool isMatch = Regex.IsMatch (line, @"^(\d{1,3}:\d{2}\s+)");

            if (isMatch && ContainsParsableTime(line)) {
                DateTime time = ParseTimeFrom (line);

                time = date.Add (time.TimeOfDay);

                currentEntry = new LogEntry {
                    Text = ParseTextFrom (line),
                    Time = time,
                };

                list.Add (currentEntry);
            } else {
                currentEntry.Text += "\n" + line;
            }
        }

        static bool ContainsParsableTime (string line)
        {
            DateTime result;

            return DateTime.TryParse (TimePart (line), out result);
        }

        static DateTime ParseTimeFrom (string line)
        {
            DateTime result;

            DateTime.TryParse (TimePart (line), out result);

            return result;
        }

        static string TimePart (string line)
        {
            return line.Substring (
                0, 
                line.IndexOf(" ", StringComparison.CurrentCultureIgnoreCase));
        }

        static string ParseTextFrom (string line)
        {
            return line.Substring (
                line.IndexOf (" ", StringComparison.CurrentCultureIgnoreCase) + 1);
        }
    }
}