﻿using System;
using System.IO;
using NUnit.Framework;
using TimeWaster.Common;
using TimeWaster.SystemAdapters;

namespace TimeWaster.IntegrationTests
{
    [TestFixture]
    public class Test
    {
        [Test]
        public void FileExists ()
        {
            bool fileExists = File.Exists ("2016-11-11 Friday.txt");
            Assert.That (fileExists, Is.True);
        }

        [Test]
        public void Parse_Folder_CountsEntries ()
        {
            var fileLoader = new SystemFileLoader ();
            var folderLoader = new SystemFolderLoader ();
            var folderParser = new FolderParser (fileLoader, folderLoader);

            var entries = folderParser.Parse ("./");

            Assert.That (entries.Count, Is.EqualTo (6));
            Assert.That (
                entries [0].Time, 
                Is.EqualTo (new DateTime(2016, 11, 11, 8, 0, 0)));
            Assert.That (
                entries [0].Text,
                Is.EqualTo ("Wake up.\nMake some tea.\n"));
            Assert.That (
                entries [1].Time,
                Is.EqualTo (new DateTime (2016, 11, 11, 9, 0, 0)));
            Assert.That (
                entries [1].Text,
                Is.EqualTo ("Study Chinese until 09:30"));            
        }
    }
}
