﻿using System;
using NUnit.Framework;
namespace TimeWaster.Common.UnitTests
{
    [TestFixture]
    public class LogParserTest
    {
        [Test]
        public void Parse_Null_ReturnsEmpty ()
        {
            var parser = new LogParser();

            Assert.That (parser.Parse (null, DateTime.MinValue).Count, Is.EqualTo(0));
        }

        [Test]
        public void Parse_1300_Hello_ReturnsHello ()
        {
            var parser = new LogParser ();

            Assert.That (
                parser.Parse ("13:00 Hello", DateTime.MinValue)[0].Text, 
                Is.EqualTo ("Hello"));
        }

        [Test]
        public void Parse_1300_HelloThere_ReturnsHelloThere ()
        {
            var parser = new LogParser ();

            Assert.That (
                 parser.Parse ("13:00 Hello There", DateTime.MinValue)[0].Text,
                 Is.EqualTo ("Hello There"));
        }

        [Test]
        public void Parse_1300_HelloThere_Returns1300 ()
        {
            var parser = new LogParser ();

            Assert.That (
                 parser.Parse ("13:00 Hello There", DateTime.MinValue)[0].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan(13,0,0)));
        }

        [Test]
        public void Parse_900_Returns_0900 ()
        {
            var parser = new LogParser ();

            Assert.That (
                 parser.Parse ("9:00 Hello There", DateTime.MinValue) [0].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (9, 0, 0)));            
        }

        [Test]
        public void Parse_2359_Then_2400_ReturnsJust1300 ()
        {
            var parser = new LogParser ();
            var actual = parser.Parse (
                "13:00 Hello There\n24:00 This line not time",
                DateTime.MinValue);

            Assert.That (actual.Count, Is.EqualTo (1));

            Assert.That (
                actual[0].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (13, 0, 0)));

            Assert.That (
               actual [0].Text,
                Is.EqualTo ("Hello There\n24:00 This line not time"));
        }

        [Test]
        public void Parse_TwoEntries_ReturnsTwo()
        {
            var result = new LogParser ()
                .Parse ("13:00 Hello There"
                            + "\n14:02 World Around Me", 
                        DateTime.MinValue);

            Assert.That (result.Count, Is.EqualTo (2));
            
            Assert.That (
                result[0].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (13, 0, 0)));

            Assert.That (
               result [0].Text,
                Is.EqualTo ("Hello There"));
            
            Assert.That (
               result[1].Time.TimeOfDay,
               Is.EqualTo (new TimeSpan (14, 2, 0)));

            Assert.That (
               result [1].Text,
               Is.EqualTo ("World Around Me"));
        }

        [Test]
        public void Parse_DoubleLineEntry_ReturnsBothLinesInText ()
        {
            var result = new LogParser ()
                .Parse ("13:00 Hello There"
                           + "\nThis is another line.",
                       DateTime.MinValue);

            Assert.That (result.Count, Is.EqualTo (1));

            Assert.That (
                result [0].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (13, 0, 0)));

            Assert.That (
               result [0].Text,
                Is.EqualTo ("Hello There\nThis is another line."));
        }

        [Test]
        public void Parse_TwoMultiLineEntries ()
        {
            var result = new LogParser ()
                .Parse ("13:00 Hello There"
                        + "\nThis is another line." 
                        + "\nAnd a third line."
                        + "\n14:02 Second Entry"
                        + "\nAlso multi line",
                    DateTime.MinValue);

            Assert.That (result.Count, Is.EqualTo (2));

            Assert.That (
                result [0].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (13, 0, 0)));

            Assert.That (
               result [0].Text,
                Is.EqualTo ("Hello There\nThis is another line.\nAnd a third line."));
        
            Assert.That (
                result [1].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (14, 2, 0)));

            Assert.That (
               result [1].Text,
                Is.EqualTo ("Second Entry\nAlso multi line"));
        }

        [Test]
        public void Parse_MicrosoftLineEndings ()
        {
            var result = new LogParser ()
                .Parse ("13:00 Hello There"
                        + "\r\nThis is another line."
                        + "\r\nAnd a third line."
                        + "\r\n14:02 Second Entry"
                        + "\r\nAlso multi line", 
                    DateTime.MinValue);

            Assert.That (result.Count, Is.EqualTo (2));

            Assert.That (
                result [0].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (13, 0, 0)));

            Assert.That (
               result [0].Text,
                Is.EqualTo ("Hello There\nThis is another line.\nAnd a third line."));

            Assert.That (
                result [1].Time.TimeOfDay,
                Is.EqualTo (new TimeSpan (14, 2, 0)));

            Assert.That (
               result [1].Text,
                Is.EqualTo ("Second Entry\nAlso multi line"));
        }
    }
}
