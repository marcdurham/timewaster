﻿using con = System.Console;
using System.Linq;
using TimeWaster.Common;
using TimeWaster.SystemAdapters;
using System;

namespace TimeWaster.Console
{
    class MainClass
    {
        public static void Main (string [] args)
        {
            if (args.Length == 0) {
                con.WriteLine ("Time Waster Console");
                con.WriteLine ("    Author: Marc Durham");
                con.WriteLine ("    Usage: twaster.exe <file-name>");
            } else {
                var fileLoader = new SystemFileLoader ();
                var folderLoader = new SystemFolderLoader ();
                var folderParser = new FolderParser (fileLoader, folderLoader);

                var entries = folderParser.Parse (args [0]);

                var sorted = entries.OrderBy (e => e.Time);

                LogEntry lastEntry = null;
                foreach (var entry in sorted) {
                    if (lastEntry != null) 
                        lastEntry.Duration = entry.Time.Subtract (lastEntry.Time);
                    
                    lastEntry = entry;
                }

                string filter = null;
                if (args.Length > 1)
                    filter = args[1];
                
                var filtered = sorted.Where (e => e.Text.Contains (filter));

                sorted = filtered.OrderByDescending (e => e.Time);

                con.WriteLine ("Date                Minutes Text");
                foreach (var entry in sorted) {
                    con.ForegroundColor = System.ConsoleColor.Green;
                    con.Write (
                        entry.Time.ToString ("yyyy-MM-dd HH:mm:ss ")
                        + entry.Duration.TotalMinutes.ToString ().PadLeft (7)
                        + " ");
                    con.ResetColor ();
                    con.WriteLine (entry.Text);
                }

                double totalMinutes = sorted.Sum (entry => entry.Duration.TotalMinutes);
                var totalTime = new TimeSpan (0, (int)totalMinutes, 0);

                con.WriteLine ();
                con.WriteLine ("Total Duration: " + totalTime.ToString());
            }
        }

        private static string Shorten (string input)
        {
            if (string.IsNullOrWhiteSpace (input))
                return input;

            string oneLine = input
                .Replace ("\r", " ")
                .Replace ("\n", "//");

            string shortLine = string.Empty;

            if (oneLine.Length > 40)
                shortLine = oneLine.Substring (0, 40) + "...";
            else
                shortLine = oneLine;

            return shortLine;
        }
    }
}
